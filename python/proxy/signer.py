import random, io 
from umbral import pre, keys, config, signing 

config.set_default_curve()

#2
# Generate an Umbral key pair
# --------------------------- 
# A delegating key pair and a Signing key pair. 
def get_signer_keys ():
    alices_signing_key = keys.UmbralPrivateKey.gen_key()
    alices_verifying_key = alices_signing_key.get_pubkey()
    alices_signer = signing.Signer(private_key=alices_signing_key)
    newkeys = {"privateSigningKey": alices_signing_key, "publicSigningKey": alices_verifying_key, "signer": alices_signer}
    print (newkeys) 
    
    return newkeys

get_signer_keys ()