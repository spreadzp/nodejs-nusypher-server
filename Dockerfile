FROM beevelop/nodejs-python:latest
ENV NODE_ENV production
WORKDIR /usr/src/app
COPY ["package.json", "package-lock.json*", "npm-shrinkwrap.json*", "./"]
RUN npm install -g sails && npm install && npm run pyth-inst && npm run pyth3-set && npm run umbral
COPY . .
EXPOSE 1337
CMD sails lift