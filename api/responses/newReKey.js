/**
 * newReKey.js
 *
 * A custom response.
 *
 * Example usage:
 * ```
 *     return res.newReKey();
 *     // -or-
 *     return res.newReKey(optionalData);
 * ```
 *
 * Or with actions2:
 * ```
 *     exits: {
 *       somethingHappened: {
 *         responseType: 'newReKey'
 *       }
 *     }
 * ```
 *
 * ```
 *     throw 'somethingHappened';
 *     // -or-
 *     throw { somethingHappened: optionalData }
 * ```
 */

module.exports = async function newReKey() {
  var res = this.res;
  const events = require('events');
  global.universalEmitter = new events(); 

  try {
    await sails.helpers.pythonCaller();
    global.universalEmitter.emit('getKey', 'privatkey.py');
    global.universalEmitter.addListener('newKey', function (data) {
      console.log('First getKey: ' + data); 
      var statusCodeToSet = 200;
      return res.status(statusCodeToSet).send({publicKey: data});
    });
  } catch (e) { 
    console.log('e :', e);
  } 
}; 