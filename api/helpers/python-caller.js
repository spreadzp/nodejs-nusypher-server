module.exports = {


  friendlyName: 'Python caller',


  description: '',


  inputs: {

  },


  exits: {

    success: {
      description: 'All done.',
    },

  },


  fn: async function () {
    let { PythonShell } = require('python-shell');
    const events = require('events');


  global.universalEmitter = new events('python.py'); 
    global.universalEmitter.on('getKey', () => { 
      startKeys('python.py'); 
    });

    function startKeys(pythonFileName) { 
      console.log('pythonFileName :', pythonFileName);
      const pathPyLib = process.cwd() + '/python/proxy';
      console.log('normalization : ' + pathPyLib);
      let options = {
        mode: 'text',
        pythonPath: '/usr/bin/python3',
        pythonOptions: ['-u'], // get print results in real-time
        scriptPath: pathPyLib,
        args: "['value1', 'value2', 'value3']"
      };

      PythonShell.run('privatkey.py', options, function (err, results) {
        if (err) throw err;
        // results is an array consisting of messages collected during execution
        console.log('results: %j', results);
        global.universalEmitter.emit('newKey', results)
      });
    };
  }
}
