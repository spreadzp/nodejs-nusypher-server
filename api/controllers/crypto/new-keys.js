module.exports = {

  friendlyName: 'New keys',

  description: 'Look up the specified user and welcome them, or redirect to a signup page if no user was found.',

  inputs: {
  },

  exits: {
    success: {
      responseType: 'newReKey',
      viewTemplatePath: 'pages/faq'
    },
    notFound: {
      description: 'No user with the specified ID was found in the database.',
      responseType: 'notFound'
    }
  },
  getKey: async function ( ) { 
} 
};
